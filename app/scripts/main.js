$(document).ready(function () {
  toggleSearchInput();
  toggleMainMenu();

  const mainSlider = '.main-slider';
  if ($(mainSlider).length) initMainSlider(mainSlider);

  const servicesSlider = '.services-slider-wrapper';
  if ($(servicesSlider).length) initServicesSlider(servicesSlider);

  const documentSlider = '.documents-slider';
  if ($(documentSlider).length) initDocumentsSlider(documentSlider);

  const singleSelect2 = '.js-select-single';
  if ($(singleSelect2).length) initSingleSelect2(singleSelect2);

  const map = '#map';
  if ($(map).length) initMap();

  hidePreloader();

  playCustomVideoPlayer();

  initFormFileUpload();
});

function initMap () {
  var map;

  DG.then(function () {
    map = DG.map('map', {
      center: [59.98584296581393, 30.306572020053867],
      zoom: 16,
      dragging : true,
      touchZoom: true,
      scrollWheelZoom: false,
      doubleClickZoom: true,
      boxZoom: false,
      geoclicker: true,
      zoomControl: false,
      fullscreenControl: true
    });

    DG.marker([59.985883, 30.306568]).addTo(map);
  });
}

function initSingleSelect2 (select) {
  const elementParent = $(select).closest('label');
  $(select).select2({
    minimumResultsForSearch: Infinity,
    containerCssClass: 'form-control',
    width: 'resolve',
    dropdownParent: elementParent
  });
}

function initFormFileUpload() {
  $('input[type="file"]').on('change', function () {
    let i = '';
    const val = $(this).val();
    if (val.lastIndexOf('\\')) {
      i = val.lastIndexOf('\\') + 1;
    } else {
      i = val.lastIndexOf('/') + 1;
    }
    $('.file-input .filename').text(val.slice(i));
  })
};

function hidePreloader() {
  if ($('body').hasClass('loading-page')) {
    setTimeout(function () {
      $('.preloader').fadeOut();
      setTimeout(function() {
        $('body').removeClass('loading-page');
      }, 500)
    }, 3000)
  }
}

function initServicesSlider() {

  // переключение по списку
  $('.services-slider-list a').click(function (e) {
    e.preventDefault();
    $('.services-slider-list .active').each(function (i, e) {
      $(e).removeClass('active');
    });
    $(this).parent().addClass('active');
    setSelectedService($(this));
  });

  // переключение кнопками
  $('.services-slider-wrapper .btn').click(function () {
    let $activeElement = $('.services-slider-list .active');
    let element = '';

    $('.services-slider-list .active').each(function (i, e) {
      $(e).removeClass('active');
    });

    if ($(this).hasClass('slider-btn__prev')) {
      element = $activeElement.prev();
    } else {
      element = $activeElement.next();
    }
    element.addClass('active');
    setSelectedService(element.find('a'));
  })
}

function setSelectedService ($element) {
  let $servicesItem = $('.services-item');
  $servicesItem.find('.services-item__image img').attr('src', $element.data('img'));
  $servicesItem.find('.services-item__title').text($element.data('name'));
  $servicesItem.find('.services-item__text').text($element.data('description'));
  $servicesItem.find('.services-item__text').text($element.data('description'));
  $servicesItem.find('.link-with-arrow').attr('href', $element.attr('href'));
}

function initMainSlider(selector) {
  let progressBar = $('.progress-bar').first();
  let interval;
  const swiper = new Swiper(selector, {
    loop: true,
    watchSlidesProgress: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '"></span>';
      },
      progressbarOpposite: true
    },
    autoplay: {
      delay: 10000,
      disableOnInteraction: false,
    },
    effect: 'slide',
    on: {
      slideChange: function () {
        let width = 1;
        let autoplayTime = 10000 / 100;
        clearInterval(interval);
        interval = setInterval(function () {
          if (width >= 100) {
            clearInterval(interval);
          } else {
            width++;
            progressBar.css('width', width + '%');
          }
        }, autoplayTime);
      }
    }
  })

  $('.btn-scroll-to-down a').click(function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top + 'px'
    }, {
      duration: 500,
      easing: 'swing'
    });
    return false;
  });
}

function initDocumentsSlider(selector) {
  const swiper = new Swiper(selector, {
    slidesPerView: 5,
    centeredSlides: true,
    spaceBetween: 30,
    initialSlide: 2,
    slideToClickedSlide: true,
    loop: true,
    navigation: {
      nextEl: '.sl-next',
      prevEl: '.sl-prev',
    }
  })
}

function toggleMainMenu() {
  const menuWrapper = $('.main-menu');
  const heightMenuWrapper = menuWrapper.innerHeight();
  const body = $('body');

  $('.burger-menu').click(function () {
    menuWrapper.css('display', 'flex');
    body.css({
      'height': heightMenuWrapper,
      'overflow-y': 'auto'
    });
  });

  $('.close-main-menu').click(function (event) {
    event.preventDefault();
    body.css('height', 'auto');
    menuWrapper.hide();
  });
}

function toggleSearchInput() {
  const btn = $('.toggleSearchInput');
  const parent = btn.closest('.search');
  const input = btn.siblings('input');
  const menu = $('.header__menu');

  btn.click(() => {
    input.addClass('visible');
    parent.addClass('active');
    menu.css('opacity', 0)
  });

  $(document).on('click', function (e) {
    if (input.hasClass('visible') && !parent.is(e.target) && parent.has(e.target).length === 0) {
      input.removeClass('visible');
      menu.css('opacity', 1)
      // setTimeout(function () {
        parent.removeClass('active');
      // }, 100)
    }
  })
}

function playCustomVideoPlayer() {
  $('.js_play').on('click', function(e) {
    e.preventDefault();
    $(this).siblings('.js_player')[0].src += '?autoplay=1';
    $(this).siblings('.js_player').show();
    $(this).siblings('.js_video-cover').hide();
    $(this).hide();
  });
}
