$(document).ready(function () {
  initSearchCity();
});

function initSearchCity () {
  $('#searchCity').keydown(function () {
    $.getJSON('/vendor/cities.json', function (countries) {
      const search = $('#searchCity').val();
      const $result = $('#searchCity-result');

      let output = '';
      let limit = 0;

      if (search.length < 3) {
        $result.html('');
        $result.dropdown('hide');
        return
      }

      for (let county of countries) {
        // другие страны
        if (county.id === '1001') {
          continue;
        }

        for (let area of county.areas) {

          if ($.isEmptyObject(area.areas)) {
            if (isSearchableCity(area, search)) {
              if (limit <= 10) {
                output += createCityItem(area);
              }
              limit += 1
            }
          } else {
            for (let city of area.areas) {
              if (isSearchableCity(city, search)) {
                if (limit <= 10) {
                  output += createCityItem(city);
                }
                limit += 1
              }
            }
          }

        }
      }
      if (output !== '') {
        $result.dropdown('show');
        $result.html(output);
      }
    });
  });
  $('#searchCity-result').on('click', '.dropdown-item', function () {
    const city = $(this).data('city_name');
    const $dropdown = $(this).closest('.dropdown-menu');
    $dropdown.dropdown('hide');
    $dropdown.html('');
    $dropdown.siblings('input').val(city);
  })
}

function createCityItem (city) {
  return `<button class="dropdown-item" type="button" data-city_name="${city.name}">${city.name}</button>`;
}

function correctString(str) {
  let replacer = {
    'q':'й', 'w':'ц'  , 'e':'у' , 'r':'к' , 't':'е', 'y':'н', 'u':'г',
    'i':'ш', 'o':'щ', 'p':'з' , '[':'х' , ']':'ъ', 'a':'ф', 's':'ы',
    'd':'в' , 'f':'а'  , 'g':'п' , 'h':'р' , 'j':'о', 'k':'л', 'l':'д',
    ';':'ж' , '\'':'э'  , 'z':'я', 'x':'ч', 'c':'с', 'v':'м', 'b':'и',
    'n':'т' , 'm':'ь'  , ',':'б' , '.':'ю' , '/':'.'
  };

  let replace = '';

  for(let i = 0; i < str.length; i++){
    if( replacer[ str[i].toLowerCase() ] !== undefined){

      if(str[i] === str[i].toLowerCase()){
        replace = replacer[ str[i].toLowerCase() ];
      } else if(str[i] === str[i].toUpperCase()){
        replace = replacer[ str[i].toLowerCase() ].toUpperCase();
      }

      str = str.replace(str[i], replace);
    }
  }

  return str;
}

function isSearchableCity(city, search) {
  const val = new RegExp(search, 'i');
  const correctVal = new RegExp(correctString(search), 'i');
  return city.name.search(val) !== -1
    || city.name.search(correctVal) !== -1;
}
